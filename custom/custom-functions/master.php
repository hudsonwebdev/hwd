<?php

$custom_path = get_template_directory() . '/custom/custom-functions/';

require $custom_path . 'enqueue.php';
require $custom_path . 'acf-helpers.php';
require $custom_path . 'guttenberg-helpers.php';
require $custom_path . 'google-helpers.php';