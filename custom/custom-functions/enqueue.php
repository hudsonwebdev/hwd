<?php
/**
 * Enqueue scripts and styles.
 */
function custom_scripts() {


    wp_enqueue_style( 'custom-style', get_template_directory_uri() . '/custom/dist/css/bundle.css', array(), _S_VERSION );
    

    wp_enqueue_script( 'custom-js', get_template_directory_uri() . '/custom/dist/js/bundle.js', array('jquery'), _S_VERSION, true );

   
}
add_action( 'wp_enqueue_scripts', 'custom_scripts' );