<?php

function get_terms_by_post_type( $taxonomies, $post_types ) {

    global $wpdb;

    $query = $wpdb->prepare(
        "SELECT t.*, COUNT(*) from $wpdb->terms AS t
        INNER JOIN $wpdb->term_taxonomy AS tt ON t.term_id = tt.term_id
        INNER JOIN $wpdb->term_relationships AS r ON r.term_taxonomy_id = tt.term_taxonomy_id
        INNER JOIN $wpdb->posts AS p ON p.ID = r.object_id
        WHERE p.post_type IN('%s') AND tt.taxonomy IN('%s')
        GROUP BY t.term_id",
        join( "', '", $post_types ),
        join( "', '", $taxonomies )
    );

    $results = $wpdb->get_results( $query );

    return $results;

}

function fc_count_posts_with_term($term_slug, $taxonomy,$post_type)
{
  global $wpdb;
  $term = get_term_by( 'slug', $term_slug, $taxonomy );

  $st = $wpdb->prepare("SELECT COUNT(*) FROM $wpdb->posts p
    INNER JOIN $wpdb->term_relationships tr
    ON (p.ID = tr.object_id)
    INNER JOIN $wpdb->term_taxonomy tt
    ON (tr.term_taxonomy_id = tt.term_taxonomy_id)
    WHERE
    p.post_status = 'publish' 
    AND p.post_type = '$post_type' 
    AND tt.taxonomy = %s
    AND tt.term_id = %d ",
    
    $taxonomy,
    $term->term_id);
  return $wpdb->get_var($st);
}



function _get_all_meta_values($key) {
    global $wpdb;
    $result = $wpdb->get_col( 
        $wpdb->prepare( "
            SELECT DISTINCT pm.meta_value FROM {$wpdb->postmeta} pm
            LEFT JOIN {$wpdb->posts} p ON p.ID = pm.post_id
            WHERE pm.meta_key = '%s' 
            AND p.post_status = 'publish'
            ORDER BY pm.meta_value", 
            $key
        ) 
    );

    return $result;
}