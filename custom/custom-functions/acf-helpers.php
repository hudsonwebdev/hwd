<?php

if( function_exists('acf_add_options_page') ) {
    
    acf_add_options_page();
    
}


//Google maps key for ACF - go to the client google account and create new key
function my_acf_google_map_api( $api ){
                
    $api['key'] = 'xxxxxxx';
    
    return $api;
    
}


add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');


/*--------display the acf blocks---------*/

function acf_flexible_content_loop(){


if( have_rows('page_sections') ):

   
    while ( have_rows('page_sections') ) : the_row();

        $dir =  get_template_directory() . '/components';


        if(get_row_layout() == "custom_code"){

            $phpfilename =  get_sub_field('template_name');
                    
        }else{

            $phpfilename = get_row_layout() ."/" . get_row_layout() . ".php";


        }

       


       
        if(file_exists($dir . '/' . $phpfilename)){

            include(locate_template('/custom/components/'. $phpfilename)); 

        }else{

            echo "Missing Template: " . $phpfilename;
        }


    endwhile;


endif;

}


/*--------This works with the utility-creaet-starter-acf.php file to generate layout templates---------*/

function create_starter_files(){

// Check value exists.
if( have_rows('page_sections') ):



        $fields = get_field_object('page_sections');

        foreach($fields['layouts'] as $layout){


               



                $phpfilename = "layout-" . $layout['name'] . ".php";
                $cssfilename = "_layout-" . $layout['name'] . ".scss";

                echo "Generating PHP: " . $phpfilename . "<br />";
                echo "Generating CSS: " . $cssfilename . "<br /><br />";


                $phpfilecontent = "<section class=\"layout-" . $layout['name'] . "\"> \n\n";


                foreach($layout['sub_fields'] as $sf){

                    
                    
                    if($sf['type']=='repeater'){

                     $phpfilecontent .=  "<?php if( have_rows('" . $sf['name'] . "') ): ?> \n\n";

                     $phpfilecontent .=  "  <div class=\"" . $sf['name'] . "\">\n\n";  

                     $phpfilecontent .= "   <?php while ( have_rows('" . $sf['name'] . "') ) : the_row(); ?> \n\n";

                     if(isset($sf['sub_fields'])){

                        foreach($sf['sub_fields'] as $sfsf){



                                if($sfsf['type']=='image'){

                                    $phpfilecontent .= "<?php $" . $sfsf['name'] . " = get_sub_field('" . $sfsf['name'] . "'); ?>\n\n";

                                    $phpfilecontent .= "<?php if($" . $sfsf['name'] . "){ ?>\n\n";

                                    $phpfilecontent .=  "<div class=\"" . $sfsf['name'] . "\" >\n\n";

                                    $phpfilecontent .=  "<img src=\"<?php echo $" . $sfsf['name'] . "['sizes']['large'];?>\" alt=\"<?php echo $" . $sfsf['name'] . "['alt'];?>\" /> \n\n";

                                    $phpfilecontent .=  "</div>\n\n";

                                    $phpfilecontent .=  "<?php  } ?>\n\n";


                                }else{

                                    $phpfilecontent .= "    <?php $" . $sfsf['name'] . " = get_sub_field('" . $sfsf['name'] . "'); ?> \n\n";

                                    $phpfilecontent .= "    <?php if($" . $sfsf['name'] . "){ ?> \n\n";



                                    $phpfilecontent .= "        <div class=\"" . $sfsf['name'] . "\"> \n\n";

                                    $phpfilecontent .= "        <?php echo $" . $sfsf['name'] . "; ?> \n\n";


                                    $phpfilecontent .= "        </div>\n\n";

                                    $phpfilecontent .= "    <?php } ?>\n\n";


                                }


                        }
                    }

                     $phpfilecontent .= "<?php endwhile; ?>\n\n";

                     $phpfilecontent .=  "</div>\n\n"; 

                     $phpfilecontent .=  "<?php endif; ?>\n\n";

                    }elseif($sf['type']=='image'){

                        $phpfilecontent .= "<?php $" . $sf['name'] . " = get_sub_field('" . $sf['name'] . "'); ?>\n\n";

                        $phpfilecontent .= "<?php if($" . $sf['name'] . "){ ?>\n\n";

                        

                        $phpfilecontent .=  "<div class=\"" . $sf['name'] . "\" >\n\n";



                        $phpfilecontent .=  "<img src=\"<?php echo $" . $sf['name'] . "['sizes']['large'];?>\" alt=\"<?php echo $" . $sf['name'] . "['alt'];?>\" /> \n\n";

                        $phpfilecontent .=  "</div>\n\n";

                        $phpfilecontent .=  "<?php  } ?>\n\n";


                    }else{


                        $phpfilecontent .= "<?php $" . $sf['name'] . " = get_sub_field('" . $sf['name'] . "'); ?>\n\n";

                        $phpfilecontent .= "<?php if($" . $sf['name'] . "){ ?>\n\n";

                        

                        $phpfilecontent .=  "<div class=\"" . $sf['name'] . "\" >\n\n";



                        $phpfilecontent .=  "<?php  echo $" . $sf['name'] . ";?>\n\n";

                        $phpfilecontent .=  "</div>\n\n";

                        $phpfilecontent .=  "<?php  } ?>\n\n";




                    }
                   

                    




                }

               

                $phpfilecontent .= '</section>';





                $dir =  get_template_directory() . '/custom/acf-layout-starter';

                if (!is_dir($dir)) {
                  mkdir($dir);
                }


                
                

                $cssfilecontent = '.layout-' .  $layout['name'] . '{ }';

                $phpfile = $dir . '/' . $phpfilename;

                $cssfile = $dir . '/' . $cssfilename;

                if (!file_exists($phpfile)) {

                    file_put_contents($phpfile , $phpfilecontent);

                }


                if (!file_exists($cssfile)) {

                    file_put_contents($cssfile , $cssfilecontent );

                }

        }

    else:

        echo "nothing";


    endif;

}